package com.example.recruitment_task.controller;


import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


import com.example.recruitment_task.controller.GitController;
import com.example.recruitment_task.response.RepositoryInfoResponse;
import com.example.recruitment_task.response.ResponsePreparator;
import com.example.recruitment_task.service.RepositoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.github.GHRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GitControllerTest_Junit4 {

    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    private GitController gitController;
    @MockBean
    private ResponsePreparator responsePreparator;
    @MockBean
    private RepositoryService repositoryService;

    private GHRepository ghRepository;

    private RepositoryInfoResponse repositoryInfoResponse;


    @Before
    public void setup()  {
        this.mockMvc = standaloneSetup(this.gitController).build();
/*      mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();*/
        //TODO -> CHOOSE ONE

         repositoryInfoResponse = RepositoryInfoResponse.builder()
                .cloneUrl("https://api.github.com/repos/PatrykSzymonMlynczak/HTML.git")
                .createdAt("2020-05-04T21:16:04+02:00")
                .description("egreg")
                .fullName("PatrykSzymonMlynczak/HTML")
                .stars(111)
                .build();
    }

    @Test
    public void testSearchRepo() throws Exception {
        when(repositoryService.findRepository(any(String.class),any(String.class))).thenReturn(ghRepository);
        when(responsePreparator.prepareResponse(ghRepository)).thenReturn(repositoryInfoResponse);

        mockMvc.perform(get("/repositories/PatrykSzymonMlynczak/HTML").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("fullName", is("PatrykSzymonMlynczak/HTML")))
                .andExpect(jsonPath("cloneUrl", is("https://api.github.com/repos/PatrykSzymonMlynczak/HTML.git")));
    }

    @Test
    public void testSearchRepoASync() throws Exception {
        // Mocking service
        when(repositoryService.findRepository(any(String.class),any(String.class))).thenReturn(ghRepository);
        when(responsePreparator.prepareResponse(ghRepository)).thenReturn(repositoryInfoResponse);

        MvcResult result = mockMvc.perform(get("/repositories/async/PatrykSzymonMlynczak/HTML").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(request().asyncStarted())
                .andDo(print())
                // .andExpect(status().is2xxSuccessful()).andReturn();
                .andReturn();

        mockMvc.perform(asyncDispatch(result))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("fullName", is("PatrykSzymonMlynczak/HTML")))
                .andExpect(jsonPath("cloneUrl", is("https://api.github.com/repos/PatrykSzymonMlynczak/HTML.git")));
    }
}

