package com.example.recruitment_task.exceptions;

public class RepositoryNotFoundException extends RuntimeException {

    public RepositoryNotFoundException() {
        super(String.format("User not found"));
    }
}
